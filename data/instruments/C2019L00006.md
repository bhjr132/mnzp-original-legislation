---
title: Customs Legislation Amendment (State of Israel) Regulations 2019
maker: ElectrumNS
made: 2019-05-08
urls:
- https://docs.google.com/document/d/1Gma7EsMaPDLWkkSPzb-Wi8XHSTAOzXUultyBBYLTBHQ/edit
- https://www.reddit.com/r/AustraliaSimLower/comments/bm3605/customs_legislation_amendment_state_of_israel/
made_under: Customs Act 1901
amends:
- Customs (Prohibited Exports) Regulations 1958
---

---
title: Customs Legislation Amendment (Republic of the Union of Myanmar) Regulations 2019
maker: ElectrumNS
made: 2019-03-06
urls:
- https://docs.google.com/document/d/1dkhEvz5U_U9LcCKErkcjRGvSRUX0hqz5LHw8qVGPYh8/edit
- https://www.reddit.com/r/AustraliaSimLower/comments/axxqbt/customs_legislation_amendment_republic_of_the/
made_under: Customs Act 1901
amends:
- Customs (Prohibited Exports) Regulations 1958
---

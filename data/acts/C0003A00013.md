---
title: Criminal Justice Reform (Youth Court Age) Amendment Act 2018 
bill: 58
introducer: HungGarRebel
party: ACT
portfolio: Justice
assent: 2018-07-03
commencement: 2020-07-04
urls:
- "https://reddit.com/r/ModelNZParliament/comments/8nbma0/b58_criminal_justice_reform_youth_court_age/"
amends:
- C0002A00011
---

#Criminal Justice Reform (Youth Court Age) Amendment Act 2018 

**Purpose**

The purpose of this Act is to repeal a section of the Criminal Justice Reform Act 2018 which changed the Youth Court jurisdiction from the age of 14 to 16 years, to 14 to 20 years. It also repeals a provision which meant that offenders within the Youth Court’s jurisdiction, who were charged with murder or other serious crimes, were not liable to be prosecuted for criminal offenses.

**1. Title**

This Act is the Criminal Justice Reform (Youth Court Age) Amendment Act 2018.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act amended**

This Act amends the Criminal Justice Reform Act 2018 (the **principal Act**).

**4. Section 2 repealed (Extension of the Youth Court’s jurisdiction)**

1) Repeal section 2.

---
title: Public Works (Compensation and Notice) Amendment Act 2019
bill: 210
introducer: Gregor_The_Beggar
author: 
- Gregor_The_Beggar (ACT)
party: ACT
portfolio: Infrastructure
assent: 2019-11-11
commencement: 2019-11-12
first_reading: reddit.com/r/ModelNZParliament/comments/dmbzzq/b210_public_works_compensation_and_notice/
committee: reddit.com/r/ModelNZParliament/comments/dp3tsv/b210_public_works_compensation_and_notice/
final_reading: reddit.com/r/ModelNZParliament/comments/drusdv/b210_public_works_compensation_and_notice/
amends:
- Public Works Act 1981
---

# Public Works (Compensation and Notice) Amendment Act 2019

**1 Title**

This Act is the Public Works (Compensation and Notice) Amendment Act 2019.

**2 Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3 Purpose**

This Act’s purpose is to legally bind the Crown to offer due and fair compensation no matter the project and to give full notice to claimants prior to the acquisition of the property.

**4 Principal Act**

The **Principal Act** refers to Public Works Act 1981

**5 Interpretation**

In this Act, unless the context otherwise requires,—

* **Notice** means notice provided in the gazette and publicly released by the Crown

* **Claimant Notice** means notice offered to a claimant prior to the public announcement of a Public Works project in direct writing.

**6 Section 23 Amended**

Section 23(c) to read “serve a claimant notice on the owner of, and persons with a registered interest in, the land of the intention to take the land in the form set out in Schedule 1 at a date no earlier than 8 weeks prior to public announcement via Section 23(b)”

**7 Section 61 amended**

Section 61(1) is removed from the principal Act

**8 Section 72 amended**

Section 72 (1) to read "Compensation of $50,000 must be paid to the owner of land if— "

Section 72 (1B) is removed from the principal act.

**9 Section 72C Amended**

Section 72C (2)(a) to read "equal to 20% of total land value.

Sections 72C (2)(b), 72C (2)(c) and 72C(3) to be removed from the principal act.

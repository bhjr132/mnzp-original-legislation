---
title: KiwiFund Act 2018
bill: 56
introducer: alpine-
party: Reform
portfolio: Member's Bills
assent: 2018-09-22
commencement: 2018-09-23
urls:
- "https://reddit.com/r/ModelNZParliament/comments/9a5dsi/b56_kiwifund_bill_third_reading/"
amends:
- KiwiSaver Act 2006
---

#KiwiFund Act 2018

**1. Title**

This Act is the KiwiFund Act 2018.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Purpose**

The purpose of this Act is to establish a government-owned and operated KiwiSaver provider, to be called KiwiFund, and to make KiwiFund the sole default KiwiSaver provider.

**4. Principal Act**

This Act amends the KiwiSaver Act 2006 (the **principal Act**).

**5. Section 4 amended (Interpretation)**

1) In section 4(1), repeal the definitions of **default investment product**, **default KiwiSaver provider**, **default KiwiSaver scheme**, and **Minister**.

2) Insert, in its appropriate alphabetical order:  

***

> **default investment product**, in relation to KiwiFund, means the investment product specified as the default investment product of the scheme as determined by KiwiFund  
> 
> **KiwiFund** means the government-owned and operated default KiwiSaver provider as established in Part 6  
> 
> **Minister**, in Part 4 and 6, sections 228 and 230, and Schedule 1,—  
> 
> > a) means the Minister of the Crown who, under the authority of any warrant or with the authority of the Prime Minister, is for the time being responsible for the administration of Part 4 and Schedule 1; and  
> > 
> > b) includes, for the purposes of subpart 2 of Part 4 (and any regulations made under section 228 or 230 for the purposes of a matter dealt with in that subpart), any Ministers of the Crown who are jointly responsible for making an appointment under section 132 (if more than 1 Minister is authorised to act jointly)

***

**6. Sections 50 to 52 repealed (Default KiwiSaver schemes)**

1) Repeal sections 50 to 52.

**7. Sections 132 to 138 repealed (Default KiwiSaver schemes)**

1) Repeal sections 132 to 138.

**8. New Part 6 (KiwiFund)**

1) Insert after Part 5:

***

> ## Part 6: KiwiFund
> 
> **241. Fund objectives**
> 
> 1) KiwiFund is the sole default KiwiSaver provider that—  
> 
> > a) is fully owned by the Government;  
> > 
> > b) is fully operated by New Zealand residents on behalf of the Government;  
> > 
> > c) resides fully in New Zealand;  
> > 
> > d) invests solely in New Zealand infrastructure, companies, bonds, cash, and other investment assets fully resident and owned in New Zealand;  
> > 
> > e) provides KiwiSaver schemes of varying investment strategies to help New Zealand residents and citizens save for their first-home or retirement; and  
> > 
> > f) operates entirely not-for-profit.
>
> **242. Fund established**
> 
> 1) On the commencement of this Act, the Minister must consult with New Zealand financial experts and entrepreneurs to establish KiwiFund in compliance with the objectives set out in section 241(1).
> 
> 2) KiwiFund must be in a position to accept contributions as a KiwiSaver provider no later than 6 months after the commencement of this Act.
> 
> **243. Fund to be the default KiwiSaver provider**
> 
> 1) This section applies, in respect of a person who is an employee of an employer and their employment with that employer, when the Commissioner has received from the employer,—  
>   
> > a) notice under section 23 of the person’s automatic enrolment; or   
> > 
> > b) notice under section 34(3) of a person’s opt-in under section 34(1)(b).
> 
> 2) However, this section does not apply to a person referred to in subsection (1)(a) or (b)—   
> 
> > a) who is an employee of an employer whose chosen KiwiSaver scheme is effective under section 47; or  
> > 
> > b) who has opted out; or  
> > 
> > c) if the Commissioner has been notified by a provider that the person has become a member of a KiwiSaver scheme.  
> 
> 3) As soon as practicable, the Commissioner must, in respect of the person’s employment with the employer,—   
> 
> > a) allocate the person to the default investment product of KiwiFund; and  
> > 
> > b) give notice to the person of that allocation; and  
> > 
> > c) send to the person the product disclosure statement relating to that product in KiwiFund; and  
> > 
> > d) give notice to the person of what will happen if the person does not choose his or her own KiwiSaver scheme.  
> 
> 4) Subsection (3) also applies, with necessary modifications, and as provided in section 57 in cases to which that section applies, to a person when— 
>   
> > a) the Commissioner receives notice under section 58 of the person having ceased to be eligible to be a member of his or her employer’s chosen KiwiSaver scheme; or   
> > 
> > b) the Commissioner receives notice under section 131(1); or  
> > 
> > c) the Commissioner receives notice of any other situation where a person is not, or is no longer, eligible to become or be a member of a certain KiwiSaver scheme and needs to be allocated to a KiwiSaver scheme under this section in order to comply with this Act.

***

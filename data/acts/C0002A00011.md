---
title: Criminal Justice Reform Act 2018
introducer: KatieIsSomethingSad
party: Labour
portfolio: Law
assent: 2018-03-06
commencement: 2018-03-07
urls:
- "https://reddit.com/r/ModelNZParliament/comments/81s43m/b20_criminal_justice_reform_bill_2018_third/"
amends:
- Children, Young Persons and Their Families Act 1989
repeals:
- Bail Amendment Act 2013
- Electoral (Disqualification of Sentenced Prisoners) Amendment Act 2010
---

#Criminal Justice Reform Act 2018

**1. Purpose**

The purpose of this Act is to modernise aspects of the Criminal Justice system and to put focus on rehabilitation rather than punishment. 

**2. Commencement**

1) This Act comes into force the day after it receives royal assent.

**3. Extension of the Youth Court’s jurisdiction**

1) In section 2(1) of the Children, Young Persons and Their Families Act 1989, replace the definition of **young person** with:

> **young person** means a person of or over the age of 14 years but under 21 years and also has an extended meaning that includes some young adults for certain purposes under section 386AAA

2) Section 272 of the Children, Young Persons and Their Families Act 1989 is repealed.

**4. Repeal of the Bail Amendment Act 2013**

1) The Bail Amendment Act 2013 is repealed.

**5. Repeal of the Electoral (Disqualification of Sentenced Prisoners) Amendment Act 2010**

1) The Electoral (Disqualification of Sentenced Prisoners) Amendment Act 2010 is repealed.

---
title: Consumers' Right to Know (Origin of Food) Act 2017
introducer: imnofox
party: Greens
portfolio: Business
assent: 2017-12-22
urls:
- "https://reddit.com/r/ModelNZParliament/comments/7kvcmb/b10_consumers_right_to_know_origin_of_food_bill/"
---

#Consumers' Right to Know (Origin of Food) Act 2017

**1. Purpose**

The purpose of this Act is to to provide a simple, mandatory labelling system in New Zealand that ensures consumers know from which country the food they are purchasing originates, so that they can make informed decisions about what they purchase and what they consume.

**2. Interpretation**

1) In this Act:

**country of origin** means the country in which the food was harvested, grown, or produced.

**single component foods** means food or food products that contain only one vegetable, fruit, meat, seafood, nut, grain, seed, or oil, although these may also contain water, sugar or its substitutes, salt, or other ingredients used in preserving, colouring or flavouring.

**3. Packaged food**

1) All packaged single component foods on sale in New Zealand must have a statement that clearly identifies the country of origin on the label.

**4. Unpackaged food**

1) All unpackaged single component foods on sale in New Zealand must have a statement that clearly identifies the country of origin clearly displayed in connection with the retail display of that food product.

**5. Foods to which this Act applies**

1) This Act applies to:

> a) fresh seafood, including prawns, shrimps, crabs, shellfish, cut fish, filleted fish, and seafood that has undergone any other processing including cooking, smoking, drying, pickling, freezing, canning, or coating with another food:
> 
> b) fresh and frozen meat, whole or cut, including meat that has been preserved by curing, drying, smoking, canning, or by other means:
> 
> c) fresh whole or cut fruit and vegetables:
> 
> d) canned, dried, or frozen fruit, and vegetables:
> 
> e) nuts, seeds, grains, oil and any other food, either whole or processed.

**6. Legibility of labelling**

1) If the labelling identifying the country or origin is on packaged food then the size of the type must be no less than 7 millimetres or 19.9 points.

2) If the labelling identifying the country of origin is on unpackaged food then the size of the type must be no less than 11 millimetres or 31.2 points.

**7. Offences**

1) No person shall sell any single component food:

> a) that bears or has attached to it any false or misleading statement, word, brand, picture, label, or mark purporting to indicate the country of origin of the food; or
> 
> b) that is the subject of or connected with any sign or display or other similar means of communication with any statement, word, brand, picture, or mark that is false or misleading in relation to the country of origin of the food.

2) No person shall, for the purpose of effecting or promoting the sale of a single component food, publish any advertisement relating to the food, containing any statement, word, brand, picture, label, or mark that is false or misleading in relation to the country of origin of the food.

3) Every individual who contravenes any of the provisions of this section commits an offence and is liable:

> a) in any case where the court is satisfied that the individual intended to commit the offence, to a fine not exceeding $8,000:
> 
> b) in any other case, to a fine not exceeding $4,000.

4) Every body corporate who contravenes any of the provisions of this section commits an offence and is liable:

> a) in any case where the court is satisfied that the body corporate intended to commit the offence, to a fine not exceeding $40,000:
> 
> b) in any other case, to a fine not exceeding $8,000.

**8. Enforcement**

The provisions of subpart 6 of Part 4 of the Food Act 2014 apply to the enforcement of the provisions of this Act, with any necessary modifications.

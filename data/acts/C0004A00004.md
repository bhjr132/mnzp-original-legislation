---
title: Electorate Electoral Referendum Act 2018
bill: 55
introducer: alpine-
party: Reform
portfolio: Member's Bills
assent: 2018-08-17
urls:
- "https://reddit.com/r/ModelNZParliament/comments/96t2f0/b55_electorate_electoral_reform_bill_third_reading/"
---

#Electorate Electoral Referendum Act 2018

**1. Purpose**

The purpose of this Act is to create the provisions for two binding referendums to be held regarding how electorate MPs are chosen. The referendums are to be held at the next general election following this Act receiving Royal assent.

**2. Provision of referendums**

1) A first referendum must be held concurrent with the next general election following the date of Royal assent on the question set out in Section 3(1).

2) A second referendum must be held concurrent with the next general election following the date of Royal assent on the question set out in Section 3(3).

**3. Questions to electors**

1) The question set out in the first referendum must be: “Should the First Past the Post electoral system used for electorates in elections be changed?

2) The possible responses to the first referendum must be:

> a) “Yes”; or
> 
> b) “No”

3) The question set out in the second referendum must be: “If the electoral system used for electorates in elections was changed, which electoral system would you prefer as the replacement?”

4) The possible responses to the second referendum must be:

> a) “Approval voting (AV)”; or
> 
> b) “Single transferable vote (STV)”; or
> 
> c) “Preferential voting (PV)”

**4. Eligible electors**

1) Every eligible elector as determined by section 3 of the Electoral Act 1993 is qualified to vote at the referendums.

**5. Electoral system of referendums**

1) The first and second referendums must employ a First Past the Post voting system in determining an outcome.

**6. Outcomes of referendum results**

1) In the outcome of the first referendum question receiving a response of greater than 50% for the answer of “Yes”, the Crown is bound to:

> a) Announce the electoral system which received the plurality of votes in the second referendum as the new electoral system; and
> 
> b) Appropriately amend the Electoral Act 1993 to incorporate the new electoral system as determined in clause (a) within two months; and
> 
> c) Consult with the Electoral Commission and the Representation Commission on how best to implement the new electoral system as determined in clause (a), without compromising the assumptions listed in Schedule 1; and
> 
> d) Act in all means appropriate to introduce the new electoral system for electorates by the day before the next general election following the day of the referendum.

2) In the outcome of the first referendum question receiving a response of 50% or greater for the answer of “No”, the Crown is bound to maintain the current electoral system until the day after the next general election following the day of the referendum.

**7. Expiration**

1) This Act will expire two years after the date of referendums.

## Schedule 1. Descriptions of electorate voting systems

**Assumptions common to alternative voting systems**

* Parliament has 120 members.
* The quantity and size of electorates are to be determined by the Electoral Commission.
* The proportion of list and electorate seats in Parliament is to remain similar, if not equal.
* Māori electorates are maintained

**Current First-past-the-post voting system (FPP)**

* Each voter has 1 electorate vote.
* Each electorate elects 1 member of Parliament.
* Voters vote for one candidate.
* The winning candidate in each electorate is the one who gains the most votes.

**Approval voting system (AV)**

* Each voter has as many votes as there are candidates in their electorate.
* Each electorate elects 1 member of Parliament.
* Voters vote for as many candidates as they wish. Voters are not required to utilise all of their votes in order to indicate their preference.
* Alternatively, voters may vote once for a set list of candidates as decided in advance by a political party.
* The winning candidate in each electorate is the one who gains the most votes.

**Preferential voting system (PV)**

* Each voter has as many votes (in sequence) as there are candidates in their electorate.
* Each electorate elects 1 member of Parliament.
* Voters rank the candidates in their electorates in order of preference, for example 1, 2, 3, and so on. Voters are not required to utilise all of their votes in order to indicate their preference. Alternatively, voters may vote once for the order of preference decided in advance by a political party.
* The winning candidate must receive a majority of preference votes, considered in order.
* If no candidate holds a majority of first preference votes, the candidate whom received the least first preference votes is eliminated. Those votes are redistributed in line with the voters' second preferences. The process repeats until a candidate holds a majority of first preference votes or redistributed votes.

**Single transferable vote (STV)**

* Each voter has as many votes (in sequence) as there are candidates in their electorate.
* Each electorate elects several members of Parliament.
* Voters rank the candidates in their electorates in order of preference, for example 1, 2, 3, and so on. Voters are not required to utilise all of their votes in order to indicate their preference. Alternatively, voters may vote once for the order of preference decided in advance by a political party.
* To win, a candidate must receive a minimum number of votes. The minimum number of votes is determined by a formula based on the number of seats allocated to the electorate.
* Any candidate who receives more than the minimum number of first-preference votes is elected. If vacancies remain, the first-preference votes received by the elected candidates that are above the minimum required for their election are redistributed according to the second preferences. The redistribution starts with the largest surplus of votes.
* If there are still vacancies after the distribution of surplus first-preference votes, the lowest-polling candidate is eliminated and all that candidate's votes are redistributed in line with the voters' second preferences, and so on. Any surplus votes from an elected candidate that were transferred to the lowest-polling candidate are redistributed in line with voters’ third preferences.
* If no candidate receives the minimum number of first-preference votes, the lowest-polling candidate is eliminated and all that candidate's votes are redistributed in line with the second preferences of the voters, and so on.

---
title: Bill of Rights (Basic Right of Privacy) Amendment Act 2019
bill: 229
introducer: PineappleCrusher_
author: 
- Gregor_The_Beggar (ACT)
party: National
portfolio: Justice
assent: 2019-12-24
commencement: 2020-03-24
first_reading: reddit.com/r/ModelNZParliament/comments/e1u3ny/b229_bill_of_rights_basic_right_to_privacy/
committee: reddit.com/r/ModelNZParliament/comments/e4v0as/b229_bill_of_rights_basic_right_to_privacy/
final_reading: reddit.com/r/ModelNZParliament/comments/eca933/b229_bill_of_rights_basic_right_to_privacy/
amends:
- Bill of Rights Act 1991
---

# Bill of Rights (Basic Right of Privacy) Amendment Act 2019

**1. Title**

This Act is the Bill of Rights (Basic Right of Privacy) Amendment Act 2019.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal Assent.

**3. Purpose**

This Act’s purpose is to include a basic right to privacy within the Bill of Rights Act 1991.

**4. Principal Act**

The Principal Act refers to the Bill of Rights Act 1991.

**5. Section 18A added**

After section 18, insert:

***

> **18A. Privacy**
> 
> Everyone has the right not to be subject to arbitrary or unlawful interference with that person’s privacy, family, home or correspondence.

***

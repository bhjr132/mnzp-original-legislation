---
title: Election Access Fund Act 2018
bill: 91
introducer: imnofox
party: Greens
portfolio: Social Development
assent: 2018-12-12
commencement: 2018-12-13
first_reading: reddit.com/r/ModelNZParliament/comments/9ugvvv/b91_election_access_fund_bill_first_reading/
committee: reddit.com/r/ModelNZParliament/comments/9xj39s/b91_election_access_fund_bill_committee/
final_reading: reddit.com/r/ModelNZParliament/comments/a0xgz0/b91_election_access_fund_bill_final_reading/
amends:
- Electoral Act 1993
---

# Election Access Fund Act 2018

**1. Title**

This Act is the Election Access Fund Act 2018.

**2. Commencement**

This Act comes into force the day after it receives Royal assent.

**3. Purpose**

The purpose of this Act is to establish a fund designed to remove or reduce barriers to standing as a candidate in a general election or otherwise participating in a general election, faced by individuals as a consequence of their disability, which non-disabled individuals do not face.

**4. Interpretation**

In this Act, unless the context otherwise requires,—

**candidate** has the same meaning as in section 3(1) of the Electoral Act 1993

**donation** has the same meaning as in section 207(1) of the Electoral Act 1993

**election education event** means an event held in relation to a general election which has the purpose of engaging and educating voters and which does not seek or discourage support for the election of a particular person, party, or people

**Electoral Commission** has the same meaning as in section 3(1) of the Electoral Act 1993

**entity** has the same meaning as in section 4(1) of the Charities Act 2005

**Fund** means the Fund established under section 6 of this Act

**general election** has the same meaning as in section 3(1) of the Electoral Act 1993

**Minister** means the Minister of the Crown who, under the authority of any warrant or with the authority of the Prime Minister, is for the time being responsible for the administration of this Act

**party** has the same meaning as in section 3(1) of the Electoral Act 1993.

**5. Act binds the Crown**

This Act binds the Crown.

**6. Election Access Fund established**

1) The Electoral Commission must establish a fund to facilitate the participation of persons with disabilities in general elections.

2) In each year, out of money appropriated by Parliament for the purpose, the fund must be paid a general grant.

3) The amount of the grant must be determined by the Minister, taking into account the amount of funding required to ensure that persons with disabilities have access to sufficient support to overcome disability-related barriers to participation in general elections.

**7. Eligibility for funding for election access**

1) The Electoral Commission must, by notice in the Gazette, set out the basis on which a person listed in subsection (3) may be eligible for a payment out of the Fund.

2) The Electoral Commission must not issue a notice under subsection (1) without first consulting persons and organisations that the Commission considers appropriate, having regard to the purpose of the Fund.

3) A person may be eligible for a payment out of the Fund if they are—  

> a) a person with a disability who is standing as a candidate in, or seeking selection as a candidate in, a general election:
> 
> b) a not-for-profit entity that is organising an election education event:
> 
> c) a party.

**8. Payment from Fund not a candidate or party donation**

Any payment made from the Fund to a candidate or a party is not to be treated as a donation for the purposes of the Electoral Act 1993.

**9. Evaluation**

The Minister must, no later than 3 years after the commencement of this Act, review, or arrange for the review of, its operation and—

> a) consider the impacts of funding on the participation of persons with disabilities in general elections; and
> 
> b) assess whether any changes are needed, including any amendments to this Act, to improve the effectiveness of funding to increase the participation of persons with disabilities in general elections; and
> 
> c) report the findings of the review to the House of Representatives as soon as practicable after the review is completed.

**10. Amendment to Electoral Act 1993**

1) This section amends the Electoral Act 1993.

2) After section 5(c), insert: > * (ca) administer the fund established by section 6 of the Election Access Fund Act 2018.

3) In section 207(1)(2), in the definition of 'candidate donation', in (b)(ii), append '; and'.

4) In section 207(1)(2), in the definition of 'candidate donation', after (b)(ii), insert:

***

> (iii) any payment made under section 6 of the Election Access Fund Act 2018

***

5) In section 207(1)(2), in the definition of 'party donation', in (b)(iii), append '; and'.

6) In section 207(1)(2), in the definition of 'party donation', after (b)(iii), insert:

***

> (iv) any payment made under section 6 of the Election Access Fund Act 2018

***

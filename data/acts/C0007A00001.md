---
title: Coroners (Tikanga and Cultural Expectations) Amendment Act 2019
bill: 148
introducer: AnswerMeNow1
author:
- imnofox (Greens)
party: Greens
portfolio: Māori Affairs
assent: 2019-05-29
commencement: 2019-05-30
first_reading: reddit.com/r/ModelNZParliament/comments/bn946g/b148_coroners_tikanga_and_cultural_expectations/
committee: reddit.com/r/ModelNZParliament/comments/bpnz6v/b148_coroners_tikanga_and_cultural_expectations/
final_reading: reddit.com/r/ModelNZParliament/comments/brzxch/b148_coroners_tikanga_and_cultural_expectations/
amends:
- Coroners Act 2006
---

# Coroners (Tikanga and Cultural Expectations) Amendment Act 2019

**1. Title**

This Act is the Coroners (Tikanga and Cultural Expectations) Amendment Act 2019.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act**

This Act amends the Coroners Act 2006 (the **principal Act**).

**4. Section 26 amended (Matters to be taken into account under section 25(3))**

After section 26(2)(3), insert:

***

> ea) the ethnic origins, social attitudes or customs, or spiritual beliefs of the person who is, or of a person who is suspected to be, the dead person, or of an immediate family member of that person, that customarily require viewing, touching, or remaining with or near the body (for example, the customary requirement that immediate family members be able to view, touch, or remain with or near the body according to tikanga Māori):

***

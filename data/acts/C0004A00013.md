---
title: Corrections (Prisoner Rehabilitation Reform) Amendment Act 2018 
bill: 70
introducer: Fresh3001
party: Liberals
portfolio: Justice
assent: 2018-09-13
commencement: 2018-09-14
urls:
- "https://reddit.com/r/ModelNZParliament/comments/95hc3l/b70_corrections_prisoner_rehabilitation_reform/"
amends:
- Corrections Act 2004
---

# Corrections (Prisoner Rehabilitation Reform) Amendment Act 2018 

**Purpose**

The purpose of this Act is to promote prison rehabilitation by reducing the sentences of prisoners who complete literacy, numeracy and trade certification programmes.

**1. Title**

This Act is the Corrections (Prisoner Rehabilitation Reform) Amendment Act 2018.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act amended**

This Act amends the Corrections Act 2004 (the **principal Act**).

**4. Subheadings amended**

1) Subheading Assessment, use of time, management plans, and programmes is amended to read the following:

***

> Assessment, use of time and management plans

***

2) The following subheading is inserted above section 52:

***

> Rehabilitation programmes

***

**5. Section 3 amended (Interpretation)**

1) In section 3 (1) insert in the appropriate alphabetical order:

***

> **serious violent offence** has the meaning given to it in section 2 (1) of the Crimes Act 1961.

***

**6. Section 52 amended**

1) Section 52 of the Corrections Act 2004 is replaced with the following:

***

> The chief executive must ensure that, to the extent consistent with the resources available and any prescribed requirements or instructions issued under section 196, rehabilitative programmes are provided to those prisoners sentenced to imprisonment under the provisions of sections 52A to 52D.

***

**7. New section 52A inserted (Eligibility for rehabilitation programmes)**

1) After section 52, insert the following:

***

> **52A Eligibility for rehabilitation programmes**
> 
> (1) Prisoners are eligible to participate in rehabilitation programmes described in section 52B if—
> 
> > (a) they pass a security assessment conducted at the discretion of the chief executive or a prison manager; and
> > 
> > (b) have not been charged with an offence contained within section 128 of this Act whilst serving their sentence within the past six months of their imprisonment; and
> > 
> > (c) have not already achieved the qualifications, or equivalent qualifications, which may be attained under section 52B; and
> > 
> > (e) have more than 3 months left of their sentence to serve.
> 
> (2) Prisoners are eligible to participate in rehabilitation programmes described in section 52C if—
> 
> > (a) they pass a security assessment conducted at the discretion of the chief executive or a prison manager; and
> > 
> > (b) were not convicted of a serious violent offence; and
> > 
> > (c) have not been charged with an offence contained within section 128 of this Act whilst serving their sentence within the past year of their imprisonment; and
> > 
> > (d) have not already achieved the qualifications, or equivalent qualifications, which may be attained under section 52C; and
> > 
> > (e) have more than 6 months left of their sentence to serve.

***

**8. New section 52B inserted (Literacy and numeracy programmes)**

1) After section 52A, insert the following:

***

> **52B Literacy and numeracy programmes**
> 
> (1) The chief executive must ensure that literacy and numeracy programmes are offered to prisoners eligible under section 52A (1).
> 
> (2) Literacy programmes offered to prisoners must—
> 
> > (a) follow the framework devised by the New Zealand Qualifications Authority; and
> > 
> > (b) allow for prisoners to achieve NCEA Literacy requirements at all levels; and
> > 
> > (c) utilise funds allocated for that expressed purpose from the corrections budget.
> 
> (3) Numeracy programmes offered to prisoners must—
> 
> > (a) follow the framework devised by the New Zealand Qualifications Authority; and
> > 
> > (b) allow for prisoners to achieve NCEA Numeracy requirements at all levels; and
> > 
> > (c) utilise funds allocated for that expressed purpose from the corrections budget.

***

**9. New section 52C inserted (Trade certification programmes)**

1) After section 52B, insert the following:

***

> **52C Trade certification programmes**
> 
> (1) The chief executive must ensure that trade certification programmes are offered to prisoners eligible under section 52A (2).
> 
> (2) The Minister must compile a list of National Certificates which may be attained by prisoners partaking in trade certification programmes. This list must consider—
> 
> > (a) the feasibility of the chief executive providing each programme within the confines of a corrections prison; and
> > 
> > (b) the demand for the qualifications offered by each programme in the labour market; and
> > 
> > (c) whether the corrections budget provides sufficient funding for each programme to occur.
> 
> (3) Trade certification programmes offered to prisoners must—
> 
> > (a) follow the framework devised by the New Zealand Qualifications Authority; and
> > 
> > (b) allow for prisoners to attain specific National Certificates as determined by the Minister; and
> > 
> > (c) utilise funds allocated for that expressed purpose from the corrections budget.
> 
> (4) A prisoner may only attain one National Certificate listed by the Minister under section 52C (2), but may attain that certificate at all levels offered.

***

**10. New section 52D inserted (Chief executive must report to Minister)**

1) After section 52C, insert the following:

***

> **52D Chief executive must report to Minister**
> 
> (1) The chief executive must prepare a report to present to the Minister annually, including—
> 
> > (a) the number of prisoners taking part in the programmes established by this section; and
> > 
> > (b) the number of prisoners achieving NCEA Literacy and Numeracy standards at all levels; and
> > 
> > (c) the number of prisoners achieving National Certificates in all fields; and
> > 
> > (d) the details of sentences which have been reduced upon the completion of these programmes, subject to section 52E of this Act.
> 
> (2) The chief executive must also present the information detailed in this section upon the request of the Minister.

***

**11. New section 52E inserted (Sentence reduction upon programme completion)**

1) After section 52D, insert the following:

***

> **52E Sentence reduction upon programme completion**
> 
> (1) A prisoner who achieves NCEA Literacy requirements at all levels offered under section 52B is eligible to have their sentence reduced by two weeks for each year of imprisonment.
> 
> (2) A prisoner who achieves NCEA Numeracy requirements at all levels offered under section 52B is eligible to have their sentence reduced by two weeks for each year of imprisonment.
> 
> (3) A prisoner who achieves a National Certificate under section 52C is eligible to have their sentence reduced by—
> 
> > (a) two weeks for each year of imprisonment for any Level 1 National Certificate attained; and
> > 
> > (b) one week for each year of imprisonment for each additional level of National Certificate attained.
> 
> (4) A prisoner may have their sentence reduced by no more than eight weeks for each year of imprisonment.

***

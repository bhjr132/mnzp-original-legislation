---
title: New Zealand Superannuation (Age of Entitlement) Amendment Act 2018
bill: 83
introducer: dyljam
party: Reform
portfolio: Social Development
assent: 2018-09-28
urls:
- "https://reddit.com/r/ModelNZParliament/comments/9hn1om/b83_new_zealand_superannuation_age_of_entitlement/"
amends:
- New Zealand Superannuation and Retirement Income Act 2001
---

# New Zealand Superannuation (Age of Entitlement) Amendment Act 2018

**Purpose**

The purpose of this Act is raise the age of eligibility for New Zealand Superannuation to 67.

**1. Title**

This Act is the New Zealand Superannuation (Age of Entitlement) Amendment Act 2018.

**2. Commencement**

This Act comes into force as follows:

1) Section 4 (1) comes into force on the 1st of January 2035 and shall expire following the commencement of section 4 (2).

2) Section 4 (2) comes into force on the 1st of January 2036 and shall expire following the commencement of section 4 (3).

3) Section 4 (3) comes into force on the 1st of January 2037 and shall expire following the commencement of section 4 (4).

4) Section 4 (4) comes into force on the 1st of January 2038.

**3. Principal Act amended**

This Act amends the New Zealand Superannuation and Retirement Income Act 2001 (the **principal Act**).

**4. Section 7 amended (Age qualification for New Zealand superannuation)**

1) Replace section 7 (1) with the following:

***

> (1) Every person is entitled to receive New Zealand superannuation who attains the age of 65 years and 6 months.

***

2) Replace section 7 (1) with the following:

***

> (1) Every person is entitled to receive New Zealand superannuation who attains the age of 66 years.

***

3) Replace section 7 (1) with the following:

***

> (1) Every person is entitled to receive New Zealand superannuation who attains the age of 66 years and 6 months.

***

4) Replace section 7 (1) with the following:

***

> (1) Every person is entitled to receive New Zealand superannuation who attains the age of 67 years.

***

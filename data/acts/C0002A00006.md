---
title: Kiwirail Emissions Reduction Act 2018
introducer: UncookedMeatloaf
party: Greens
portfolio: Environment
assent: 2018-02-21
urls:
- "https://reddit.com/r/ModelNZParliament/comments/7y10mc/b13_kiwirail_emissions_reduction_bill_2018_third/"
---

#Kiwirail Emissions Reduction Act 2018

**Purpose**

The purpose of this Act is to reduce the environmental impact of New Zealand’s passenger and freight rail transport.

**1. Title and Commencement**

1) This Act may be cited as the KiwiRail Emissions Reduction Act or abbreviated as KERA.

2) Section 3(1) shall take effect on January 1st, 2028.

3) Section 1, 2, 4, and 5 shall take effect on January 25th, 2018.

4) Section 3 shall not apply to locomotives and railcars built on or before December 31st, 1980.

**2. Interpretation**

In this Act—

**Normal operation** means standard operations of the locomotive and train excluding any emergency scenario or malfunction.

**Road power** means electrical power used for the express purpose of locomotion, as opposed to heating, computer systems, or other non-locomotive purposes.

**Pollutants** means:  
> HC - Hydrocarbon/THC emissions  
> 
> CO - Carbon Monoxide emissions  
> 
> NOx - Nitrogen Oxide emissions defined as Nitric Oxide (NO) or nitrogen dioxide (NO2).  
> 
> PM - Particulate Matter emissions defined as fine particles with a diameter of smaller than 10 micrometers.  

**New Zealand Railways Corporation** may be abbreviated as NZRC.

**Electrification** means the installation of electrical lines and infrastructure necessary for the operation of electric locomotives.

**3. General Requirements for Pollution and Emissions**

1) Operation by KiwiRail of any locomotive or railcar which meets the following standards is prohibited:  

> a) Consumption of more than 58 liters-per-hour of diesel in normal operation.  
> 
> b) The primary source of electrical and road power comes from a diesel-electric, gasoline-electric, or kerosene-electric engine.
>    
> c) Produces HC, CO, NOx, or PM in a concentration greater than ~0 g/bhp-hr.

**4. Electrification and Rail Infrastructure**

1) The New Zealand Railways Corporation is hereby ordered to commence electrification procedures on the entirety of track under its jurisdiction. Electrification must be complete on or by the date January 1st, 2028.

**5. Budget and Costing**

1) The government shall appropriate $80,000,000 to be distributed to KiwiRail and NZRC as follows:  

> a) $60,000,000 to the New Zealand Rail Corporation to be used for electrification and rail infrastructure renewal in compliance with this Act.  
> 
> b) $20,000,000 to KiwiRail to be used in compliance with this Act.

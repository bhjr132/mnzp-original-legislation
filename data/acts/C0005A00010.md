---
title: Goods and Services Tax (Budget Measures) Amendment Act 2018
bill: 101
introducer: silicon_based_life
party: Opportunities
portfolio: Finance
assent: 2018-12-12
commencement: 2018-12-13
urgent_reading: reddit.com/r/ModelNZParliament/comments/a2gxe9/b101_goods_and_services_tax_budget_measures/
amends:
- Goods and Services Tax Act 1985
---

# Goods and Services Tax (Budget Measures) Amendment Act 2018

**1. Title**

This Act is the Goods and Services Tax (Budget Measures) Amendment Act 2018.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal act**

This Act amends the Goods and Services Tax Act 1985 (the **principal Act**).

**4. Section 8 amended (Imposition of goods and services tax on supply)**

1) In section 8(1), “15” is replaced by “14”.

2) Subsection (1) applied to supplies made on or after 1 December 2018.

**5. Section 12 amended (Imposition of goods and services tax on imports)**

1) In section 12(1), “15” is replaced by “14”.

2) Subsection (1) applied to the importation of goods on or after 1 December 2018.
